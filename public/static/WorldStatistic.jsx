const serverUrl = window.location.origin + '/index.php';

class WorldStatistic extends React.Component {
    serialize = (obj, prefix) => {
        let str = [],
            p;
        for (p in obj) {
            if (obj.hasOwnProperty(p)) {
                let k = prefix ? prefix + "[" + p + "]" : p,
                    v = obj[p];
                str.push((v !== null && typeof v === "object") ?
                    this.serialize(v, k) :
                    encodeURIComponent(k) + "=" + encodeURIComponent(v));
            }
        }
        return str.join("&");
    };

    constructor(props) {
        super(props);
        this.state = {
            regions: [],
            orderBy: [],
            fixedLanguages: false,
            columns: ['Continent', 'Region', 'Countries', 'LifeDuration', 'Population', 'Cities', 'Languages']
        }
    }

    componentDidMount() {
        this.refreshRegions();
    }

    refreshRegions = () => {
        fetch(serverUrl, {
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            body: this.serialize({
                _p: 'statistic/world',
                orderBy: this.state.orderBy,
                fixedLanguages: this.state.fixedLanguages
            })
        }).then((response) => response.json()).then((regions) => {
            this.setState({regions: regions});
        })
    };

    sortTableHandler = (column) => {
        let orderBy = this.state.orderBy;
        const columnIndex = this.state.orderBy.findIndex((item) => {
            return item.hasOwnProperty(column)
        });
        let value = {};
        if (columnIndex === -1) {
            value[column] = 'ASC';
            orderBy.push(value);
        } else if (orderBy[columnIndex][column] === 'ASC') {
            orderBy[columnIndex][column] = 'DESC';
        } else {
            orderBy = orderBy.filter((value, index) => {
                return index !== columnIndex
            });
        }
        this.setState({orderBy: orderBy}, () => {
            this.refreshRegions();
        });
    };

    fixedLanguagesToggle = () => {
        this.setState({fixedLanguages: !this.state.fixedLanguages}, () => {
            this.refreshRegions();
        });
    };

    getSortIndexDirection(column) {
        let orderBy = this.state.orderBy;
        const columnIndex = this.state.orderBy.findIndex((item) => {
            return item.hasOwnProperty(column)
        });
        if (columnIndex === -1) {
            return;
        }

        let label;
        if (orderBy[columnIndex][column] === 'DESC') {
            label = <span className='nobr'>[{columnIndex + 1} &#8593;]</span>;
        } else {
            label = <span className='nobr'>[{columnIndex + 1} &#8595;]</span>;

        }

        return label;
    }

    render() {
        const rows = this.state.regions.length > 0 ? this.state.regions.map((item, index) => {
            return <tr key={'row_' + index}>{this.state.columns.map((column, columnIndex) => {
                return <td key={'column_' + index + '_' + columnIndex} className={!isNaN(item[column]) ? 'numeric' : ''}>{item[column] ? item[column] : '-'}</td>
            })}
            </tr>
        }) : (<tr>
            <td>No items received</td>
        </tr>);

        const header = this.state.columns.map((item, index) => {
            return <th key={'header_' + index} onClick={() => {
                this.sortTableHandler(item)
            }}>{item} {this.getSortIndexDirection(item)}
            </th>;
        });

        return <div>
            <h1 className={'text-center'}>World database</h1>
            <div className={'text-center'}>Fix languages count:
                <input type={'checkbox'} key={'fix_languages'} onChange={this.fixedLanguagesToggle}
                       checked={this.state.fixedLanguages}/>
            </div>
            <table>
                <thead>
                <tr>
                    {header}
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        </div>;
    }
}
