<?php
require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'app', 'App.php']));
require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'app', 'Config.php']));

use App\App;

// Create App
$app = new App();

// Run app
$app->run();
