<?php

namespace App\Controller;
use App\Http\HtmlFileResponse;
use App\Repository\WorldRepository;
use App\Http\JsonResponse;

/**
 * Statistic controller.
 * @controller()
 * @route('statistic')
 */
class Statistic extends AbstractController
{
    /**
     * Index page
     * @route('index')
     */
    public function index(){
        return new HtmlFileResponse('index.html');
    }

    /**
     * World statistic as JSON
     * @route('world')
     */
    public function world(){
        $orderBy = $_REQUEST['orderBy'] ?? [];
        $fixed = $_REQUEST['fixedLanguages'] ?? '';
        $repo = new WorldRepository();
        $rows = $repo->getWorldStatistic($orderBy, filter_var($fixed, FILTER_VALIDATE_BOOLEAN));

        return new JsonResponse($rows);
    }
}
