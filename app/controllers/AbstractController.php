<?php

namespace App\Controller;

use App\Exceptions\HttpNotFoundException;
use App\Http\HttpResponseInterface;

class AbstractController
{
    protected $routes = [];

    /**
     * AbstractController constructor.
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->initRoute();
    }

    /**
     * Init routes from controller methods.
     * @throws \ReflectionException
     */
    protected function initRoute()
    {
        $class = new \ReflectionClass(static::class);
        // Прочитать у класса все методы, у которых есть аннтотация
        foreach ($class->getMethods() as $method) {
            $found = preg_match("/^\s+\*\s+@route\((.*?)\)/m", $method->getDocComment(), $matches);
            if ($found) {
                $this->routes[trim($matches[1], '\'"')] = $method->getName();
            }
        }

    }

    /**
     * Serve request
     * @param string $url
     * @throws HttpNotFoundException
     */
    public function serve(string $url)
    {
        if (isset($this->routes[$url])) {
            /** @var HttpResponseInterface $response */
            $response = $this->{$this->routes[$url]}();
            $response->render();
        } else {
            throw new HttpNotFoundException();
        }
    }
}
