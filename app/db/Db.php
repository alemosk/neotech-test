<?php

namespace App\Db;

use App\Config;
use mysqli;
use PDO;

class Db
{
    /** @var PDO|null */
    private $pdo;

    public function __construct()
    {
        $config = new Config();
        $host = $config->getSetting('DB_HOST');
        $port = $config->getSetting('DB_PORT');
        $name = $config->getSetting('DB_NAME');
        $user = $config->getSetting('DB_USER');
        $password = $config->getSetting('DB_PASSWORD');

        $uri = "mysql:host=$host;port=$$port;dbname=$name";
        $this->pdo = new PDO($uri, $user, $password);
    }

    /**
     * @param $sql
     * @param array $params
     * @return array
     */
    public function fetchAll(string $sql, array $params = [])
    {
        $conn = $this->getConnection();
        $statement = $conn->prepare($sql);
        foreach ($params as $key => $value) {
                $statement->bindParam($key, $value);
        }
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return PDO|mysqli
     */
    public function getConnection()
    {
        if ($this->pdo === null) {
            throw new \RuntimeException('Database connection not found');
        }
        return $this->pdo;
    }
}
