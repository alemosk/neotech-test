<?php

namespace App\Repository;


use App\Db\Db;

/**
 * Class WorldRepository
 * @package App\Repository
 */
class WorldRepository
{
    private $db;

    /**
     * WorldRepository constructor.
     */
    public function __construct()
    {
        $this->db = new Db();
    }

    /**
     * Get world statistic
     * @param array $orderBy
     * @param bool $fixed
     * @return array
     */
    public function getWorldStatistic(array $orderBy = [], bool $fixed = false)
    {
        $allowedOrderBy = ['Continent', 'Region', 'Countries', 'LifeDuration', 'Population', 'Cities'];
        $orderBy = array_filter($orderBy, function ($item) use ($allowedOrderBy) {
            $keys = array_keys($item);
            return count($keys) === 1 && in_array($keys[0], $allowedOrderBy, true);
        });

        $sql = <<<SQL
SELECT statistic.Continent,
       country.Region,
       statistic.Countries,
       ROUND(statistic.LifeDuration, 2) LifeDuration,
       statistic.Population,
       statistic.Cities,
       COUNT([__DISTINCT__]countryLanguage.Language)  Languages
FROM (SELECT subCountry.Continent,
             subCountry.Region,
             COUNT(subCountry.Code)         Countries,
             AVG(subCountry.LifeExpectancy) LifeDuration,
             SUM(subCountry.Population)     Population,
             city.Cities                    Cities
      FROM Country subCountry
               LEFT JOIN (SELECT joinCountry.Region, COUNT(*) Cities
                          FROM City subCity
                                   JOIN Country joinCountry ON subCity.CountryCode = joinCountry.Code
                          GROUP BY joinCountry.Region) city ON city.Region = subCountry.Region
      GROUP BY Region, Continent) statistic
         JOIN Country country ON country.Region = statistic.Region
         LEFT JOIN CountryLanguage countryLanguage ON countryLanguage.CountryCode = country.Code
GROUP BY country.Region, statistic.Continent, statistic.Population, statistic.Countries, statistic.Cities,
         statistic.LifeDuration
SQL;
        $sql = str_replace('[__DISTINCT__]', $fixed ? 'DISTINCT ' : '', $sql);

        $orderBySQL = null;
        for ($i = 0; $i < count($orderBy); $i++) {
            foreach ($orderBy[$i] as $key => $value) {
                if ($orderBySQL === null) {
                    $orderBySQL = ' ORDER BY ';
                    $orderBySQL .= $key . ' ' . ($value === 'DESC' ? 'DESC' : 'ASC');
                } else {
                    $orderBySQL .= ', ' . $key . ' ' . ($value === 'DESC' ? 'DESC' : 'ASC');
                }
            }
        }

        $sql .= $orderBySQL ?: '';

        return $this->db->fetchAll($sql);
    }
}
