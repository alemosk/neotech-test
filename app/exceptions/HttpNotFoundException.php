<?php

namespace App\Exceptions;

use Exception;

/**
 * Class HttpNotFoundException
 * @package App\Exceptions
 */
class HttpNotFoundException extends Exception
{

}
