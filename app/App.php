<?php

namespace App;


use App\Exceptions\HttpNotFoundException;

/**
 * Class App
 * @package App
 */
class App
{
    private $urls = [];

    /**
     * Start application
     * @throws \ReflectionException
     */
    public function run()
    {
        $this->loadApp();
        $this->loadUrl();
        $this->serveRequest();
    }

    /**
     * Load application files
     */
    private function loadApp()
    {
        $dirs = ['exceptions', 'interfaces', 'db', 'repository', 'http', 'controllers'];

        $files = [];
        foreach ($dirs as $dir) {
            $dir = implode(DIRECTORY_SEPARATOR, [__DIR__, $dir]);
            $files = array_merge($files, array_map(function ($file) use ($dir) {
                return implode(DIRECTORY_SEPARATOR, [$dir, $file]);
            }, scandir($dir)));

        }
        array_unique($files);

        foreach ($files as $file) {
            if (substr($file, -4) !== '.php' || is_dir($file)) {
                continue;
            }
            /** @noinspection PhpIncludeInspection */
            include($file);
        }
    }

    /**
     * Load urls from classes with "controller" annotation
     * @throws \ReflectionException
     */
    private function loadUrl()
    {
        $controllers = $this->findClassNamesByAnnotation('controller');
        foreach ($controllers as $key => $value) {
            $namespaceParts = explode('\\', $key);
            $route = empty($value['annotations']['route']) ? strtolower(end($namespaceParts)) : $value['annotations']['route'];
            $this->urls[trim($route, '\'"')] = $key;
        }
    }

    /**
     * Find class names with specific annotation
     * @param string $annotation
     * @return array
     * @throws \ReflectionException
     */
    private function findClassNamesByAnnotation(string $annotation): array
    {
        $appClasses = array_filter(get_declared_classes(), static function ($item) {
            return strpos($item, 'App\\') === 0;
        });

        $classes = [];
        foreach ($appClasses as $class) {
            $rc = new \ReflectionClass($class);
            $found = preg_match('/^\s+\*\s+@' . $annotation . '\(.*?\)/m', $rc->getDocComment());
            if ($found) {
                $classes[$class] = ['annotations' => $this->getClassAnnotations($class)];
            }
        }
        return $classes;
    }

    /**
     * Get annotations from class.
     * @param $className
     * @return array
     * @throws \ReflectionException
     */
    private function getClassAnnotations(string $className): array
    {
        $rc = new \ReflectionClass($className);
        $found = preg_match_all("/^\s+\*\s+@([A-z][0-9A-z_-]+)\((.*?)\)/m", $rc->getDocComment(), $matches);
        $annotations = [];
        if ($found) {
            for ($i = 0; $i < $found; $i++) {
                $annotations[$matches[1][$i]] = $matches[2][$i];
            }

        }
        return $annotations;
    }

    /**
     * Serve request
     */
    private function serveRequest()
    {
        $page = $_REQUEST['_p'] ?? 'statistic/index';
        $pageParts = explode('/', $page, 2);
        try {
            $controller = current($pageParts);
            $controllerClass = $this->urls[$controller] ?? null;
            try {
                $controllerObject = (new \ReflectionClass($controllerClass))->newInstance();
            } catch (\ReflectionException $ignored) {
                throw new HttpNotFoundException();
            }
            $controllerObject->serve(end($pageParts));
        } catch (HttpNotFoundException $ignored) {
            http_response_code(404);
            die('Page not found');
        }
    }
}
