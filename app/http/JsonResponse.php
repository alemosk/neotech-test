<?php

namespace App\Http;

/**
 * Class JsonResponse
 * @package App\Http
 */
class JsonResponse implements HttpResponseInterface
{
    private $data;

    /**
     * JsonResponse constructor.
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function render()
    {
        echo json_encode($this->data);
    }
}
