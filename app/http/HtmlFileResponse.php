<?php

namespace App\Http;


class HtmlFileResponse implements HttpResponseInterface
{
    private $htmlFile;

    /**
     * HtmlResponse constructor.
     * @param $htmlFile
     */
    public function __construct(string $htmlFile)
    {
        $resourcesDirectory = implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'resources']);
        $this->htmlFile = implode(DIRECTORY_SEPARATOR, [$resourcesDirectory, $htmlFile]);
    }

    public function render()
    {
        echo file_get_contents($this->htmlFile);
    }
}
