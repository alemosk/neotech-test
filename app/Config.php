<?php

namespace App;

/**
 * Class Config
 * @package App
 */
class Config
{
    private $settings = [];

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $env = new \SplFileObject(dirname(__DIR__) . DIRECTORY_SEPARATOR . '.env');
        while (!$env->eof()) {
            $line = trim($env->fgets());

            if (strpos($line, '#') === 0) {
                continue;
            }

            $parts = explode('=', $line, 2);

            if (count($parts) === 2) {
                array_map(function ($part) {
                    return trim($part);
                }, $parts);
                $this->settings[$parts[0]] = $parts[1];
            }
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getSetting(string $name)
    {
        if (empty($this->settings[$name])) {
            throw new \RuntimeException('Setting ' . $name . ' not found');
        }

        return $this->settings[$name];
    }
}
