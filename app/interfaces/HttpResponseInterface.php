<?php

namespace App\Http;

/**
 * Interface HttpResponseInterface
 * @package App\Http
 */
interface HttpResponseInterface
{
    public function render();

}
