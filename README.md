# World statistic (NeoTech test task).

Link: [neotech.it-live.info](https://neotech.it-live.info)

## Requirements
* PHP 7+ with MySQL PDO driver.
* Docker composer (optional) 
* MySQL Server
* Browser with Internet connection for React and polyfill libs (IE support).

## Installation on Linux with docker-compose.
Project settings stores in `.env` file. 

File name with common settings is `env.dist`. 

After setup variables in the `.env` run
~~~
$ docker-compose up -d
~~~
If this is the first run we need install test data
~~~
$ docker-compose exec mysql bash
(mysql) # cd /neotech
(mysql) # mysql -h localhost -u neotech -p neotech < world.sql
~~~
And enter the database password (default password is: `neotech_test`)
> Same operation need to complete after `docker-compose down` because we don't use volumes in this project.

Now we can open application in browser (eg. http://localhost:8080).

## Notice
Application have `Fix languages count` checkbox. 

Because of screenshot in the task where is shown invalid languages count.

It means that different languages by country count correctly, 
but this data contains logic issue eg. `English` language. 
If dialect is not specified, this one is the same language in the different countries.

|Continent   	|Region   	      |Countries  |LifeDuration |Population  |Cities   	|Languages |
|---            |---              |---        |---          |---         |---       |---       |
|Europe         |British Islands  |2          |77.25        |63398500    |83        |5         |

We see that in the British Islands are five languages, but if we look at the counted data
~~~
English, Gaeli, Kymri, English, Irish
~~~
we find that English language counted twice. Correct value must be `4`, not `5`.



   
